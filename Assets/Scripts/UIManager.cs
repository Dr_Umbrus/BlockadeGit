using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public enum UIStates { Null, InGame, MainMenu, LevelSelect, PressStart, Options, InGameOptions, InGameMenu, Dialogue, EndLevel, Credits, Extra}
    
    [Header("States & Main")]
    public UIStates state=UIStates.Null;
    public GameObject gameUI, mainUI, levelSelectUI, startUI, optionsUI, inGameOptionsUI, ingameMenuUI, dialogueUI, endLevelUI, creditsUI, mainExtraUI;
    
    public PortraitNames portraitnames;
    public LeanProfiles[] profiles;

    public bool startingLevel = false;

    [Header("MainMenu")]
    public GameObject[] mainCursors;
    public ScriptablePalette mainPalette;
    public ScriptablePalette blinkPalette;
    public Image[] imagesToColor;
    int currentColor;
    public float blinkSpeed=4;
    public groupImages[] groups;
    float blinkTime;
    bool blinkUp = true;
    

    [Header("LevelSelect")]
    public GameObject[] levelCursors;
    public GameObject[] levels;
    public Image[] levelRanks;

    [Header("Options")]
    public Color darkestLight;
    public Image lightAdjustment;
    public GameObject[] optionCursers;
    public Slider optionMainSound, optionOtherSound, optionMusicSound, optionLumSlide;
    public TMP_Text optionReso;


    [Header("InGame")]
    public TMP_Text levelName;
    public TMP_Text numberOfMovesText;
    public Image currentRank;
    public Sprite[] rankSprites;
    public GameObject[] blocksTypes;
    public TMP_Text[] blocksLeft;
    public int numberofMoves=0;
    int rank = 0;

    

    [Header("InGameMenu")]
    public GameObject[] menuIGCursors;

    [Header("OptionsInGame")]
    public GameObject[] inGameOptionCursers;
    public Slider optionIGMainSound, optionIGOtherSound, optionIGMusicSound, optionIGLumSlide;
    public TMP_Text optionIGReso;

    

    [Header("Dialogues")]
    public TMP_Text currentDialogue;
    public Image portrait;
    public TMP_Text characterName;
    public GameObject choiceCursor1, choiceCursor2;
    public GameObject choiceUI;
    public TMP_Text choice1text, choice2text;
    public GameObject nextButton;

    [Header("EndLevel")]
    public GameObject[] endLevelCursers;


    [Header("Misc")]
    public Image loadingBlack;
    public bool blackening = false;
    float blackLerp = 0;
    public float blackLerpSpeed;

    Options optionManager;
    public GameManager gm;

    public SceneReader sr = null;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        optionManager = GetComponent<Options>();

        
    }

    #region state
    public void ChangeState(UIStates newState)
    {
        EndState(newState);
        NewState(newState);

        ResetMenuPos();
    }

    public void ApplyState(UIStates newState)
    {
        state = newState;
    }

    public void EndState(UIStates newState)
    {
        switch (state)
        {
            case UIStates.MainMenu:
                DeactivateUI(mainUI);
                /*if (newState == UIStates.InGame)
                {
                    DeactivateUI(mainUI);
                }
                else
                {
                    for (int i = 0; i < profiles.Length; i++)
                    {
                        if (profiles[i].profileName == Names.mainMenuOUT)
                        {
                            for (int j = 0; j < profiles[i].targets.Length - 1; j++)
                            {
                                LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type);
                            }
                            LeanTween.move(profiles[i].targets[profiles[i].targets.Length - 1], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => DeactivateUI(mainUI));
                            break;
                        }
                    }
                }*/
                break;

            case UIStates.LevelSelect:
                DeactivateUI(levelSelectUI);

                /*
                for (int i = 0; i < profiles.Length; i++)
                {
                    if (profiles[i].profileName == Names.levelSelectOUT)
                    {
                        for (int j = 0; j < profiles[i].targets.Length - 1; j++)
                        {
                            LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type);
                        }
                        LeanTween.move(profiles[i].targets[profiles[i].targets.Length - 1], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => DeactivateUI(levelSelectUI));
                        break;
                    }
                }*/
                break;

            case UIStates.PressStart:
                DeactivateUI(startUI);
                break;

            case UIStates.Options:

                /*
                for (int i = 0; i < profiles.Length; i++)
                {
                    if (profiles[i].profileName == Names.mainMenuOptionsOUT)
                    {
                        for (int j = 0; j < profiles[i].targets.Length - 1; j++)
                        {
                            LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type);
                        }
                        LeanTween.move(profiles[i].targets[profiles[i].targets.Length - 1], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => DeactivateUI(inGameOptionsUI));
                        break;
                    }
                }*/

                DeactivateUI(optionsUI);
                break;

            case UIStates.Dialogue:
                DeactivateUI(dialogueUI);
                break;


            case UIStates.InGameOptions:
                /*for (int i = 0; i < profiles.Length; i++)
                {
                    if (profiles[i].profileName == Names.inGameOptionsOUT)
                    {
                        for (int j = 0; j < profiles[i].targets.Length; j++)
                        {
                            LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => DeactivateUI(inGameOptionsUI));
                        }
                        break;
                    }
                }
                optionManager.inGameValue = 0;*/

                DeactivateUI(inGameOptionsUI);
                break;

            case UIStates.InGameMenu:
                DeactivateUI(ingameMenuUI);
                if (newState != UIStates.InGameOptions && newState != UIStates.InGame)
                {
                    DeactivateUI(gameUI);
                }

                break;

            case UIStates.EndLevel:
                DeactivateUI(endLevelUI);
                break;

            case UIStates.Extra:
                DeactivateUI(mainExtraUI);
                break;
        }

        state = UIStates.Null;
    }

    public void NewState(UIStates newState)
    {
        ApplyState(newState);

        switch (newState)
        {
            case UIStates.InGame:
                if (sr == null)
                {
                    rank = 5;
                    SetLevel();
                }

                gameUI.SetActive(true);

                if (startingLevel && (gm.currentLevel == gm.maxUnlockedLevel && gm.scriptableText.dialogues[gm.currentLevel * 2].text.Count > 0))
                {
                    ChangeState(UIStates.Dialogue);
                }
                else
                {
                    startingLevel = false;
                }
                break;

            case UIStates.MainMenu:
                UpdateMainMenu(optionManager.mainMenuValue);
                currentColor = Random.Range(0, mainPalette.col.Length);
                foreach (Image i in imagesToColor)
                {
                    i.color = mainPalette.col[currentColor];
                }
                blinkUp = true;
                blinkTime = 0;
                gameUI.SetActive(false);
                mainUI.SetActive(true);
                break;

            case UIStates.LevelSelect:
                levelSelectUI.SetActive(true);
                StartLevelSelect();
                /*for (int i = 0; i < profiles.Length; i++)
                {
                    if (profiles[i].profileName == Names.levelSelectIN)
                    {
                        for (int j = 1; j < profiles[i].targets.Length; j++)
                        {
                            LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type);
                        }
                        LeanTween.move(profiles[i].targets[0], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => ApplyState(newState));
                        break;
                    }
                }*/
                for (int i = 0; i <= gm.maxUnlockedLevel; i++)
                {
                    levels[i].SetActive(true);
                }
                for (int i = gm.maxUnlockedLevel+1; i <= gm.lastLevel; i++)
                {
                    levels[i].SetActive(false);

                    if (levels[i].activeSelf)
                    {
                        levels[i].SetActive(false);
                    }
                }
                break;

            case UIStates.PressStart:
                startUI.SetActive(true);
                break;

            case UIStates.Options:
                optionsUI.SetActive(true);
                break;

            case UIStates.Dialogue:
                dialogueUI.SetActive(true);

                if (startingLevel)
                {
                        StartDialogue(gm.currentLevel * 2);
                }
                else
                {
                        StartDialogue(gm.currentLevel * 2+1);
                }
                break;

            case UIStates.InGameOptions:
                inGameOptionsUI.SetActive(true);
                /*for (int i = 0; i < profiles.Length; i++)
                {
                    if (profiles[i].profileName == Names.inGameOptionsIN)
                    {
                        for (int j = 0; j < profiles[i].targets.Length-1; j++)
                        {
                            LeanTween.move(profiles[i].targets[j], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type);
                        }
                        LeanTween.move(profiles[i].targets[profiles[i].targets.Length - 1], profiles[i].movement, profiles[i].speed).setEase(profiles[i].type).setOnComplete(() => ApplyState(newState));
                        break;
                    }
                }*/
                optionManager.inGameValue = 0;
                UpdateInGameOptions(0);
                break;

            case UIStates.InGameMenu:
                ingameMenuUI.SetActive(true);
                UpdateInGameMenu(0);
                break;

            case UIStates.EndLevel:
                optionManager.endLevelValue = 0;
                if (rank < gm.ranks[gm.currentLevel])
                {
                    gm.ranks[gm.currentLevel] = rank;
                }
                UpdateEndPos(0);
                endLevelUI.SetActive(true);
                break;

            case UIStates.Extra:
                optionManager.extraValue = 0;
                //
                mainExtraUI.SetActive(true);
                break;
        }     
    }
    #endregion


    public void DeactivateUI(GameObject deActivate)
    {
        deActivate.SetActive(false);
    }


    private void Update()
    {
        

        if (blackening)
        {
            if (blackLerp < 1)
            {
                blackLerp += Time.deltaTime*blackLerpSpeed;
            }
        }
        else
        {
            if (blackLerp > 0)
            {
                blackLerp -= Time.deltaTime * blackLerpSpeed;
            }
        }

        loadingBlack.color = Color.Lerp(Color.clear, Color.black, blackLerp);

        /*LeanTween.moveX(gameObject, 0, 1).setOnComplete(() => DeactivateUI(gameObject));
        LeanTween.moveX(gameObject, 0, 1).setOnComplete(ChangeBlackening);*/
        if (state == UIStates.MainMenu)
        {
            if (blinkUp)
            {
                blinkTime += Time.deltaTime*blinkSpeed;
                if (blinkTime > 1) { blinkTime = 1; blinkUp = false; }
            }
            else
            {
                blinkTime -= Time.deltaTime*blinkSpeed;
                if (blinkTime <0) { blinkTime = 0; blinkUp = true; }
            }
            foreach(Image i in groups[optionManager.mainMenuValue].images)
            {
                i.color = Color.Lerp(mainPalette.col[currentColor], blinkPalette.col[currentColor], blinkTime);
            }
        }
    }


    public void ChangeBlackening()
    {
        if (blackening)
        {
            blackening = false;
            blackLerp = 1;
        }
        else
        {
            blackening = true;
            blackLerp = 0;
        }
    }

    #region MainMenu

    public void UpdateMainMenu(int pos)
    {
        blinkUp = true;
        blinkTime = 0;
        for (int i = 0; i < mainCursors.Length; i++)
        {
            mainCursors[i].SetActive(false);
        }
        mainCursors[pos].SetActive(true);

        foreach (Image i in imagesToColor)
        {
            i.color = mainPalette.col[currentColor];
        }
    }
    
    public void OpenMainMenuOptions()
    {
        UpdateMainMenuOptions(0);
        optionManager.mainMenuOptionValue = 0;
        optionsUI.SetActive(true);
        ChangeState(UIStates.Options);
    }

    public void CloseMainMenuOptions()
    {
        ChangeState(UIStates.MainMenu);
        optionManager.mainMenuValue = 2;
    }
    public void UpdateMainMenuOptions(int pos)
    {
        foreach(GameObject curse in optionCursers)
        {
            curse.SetActive(false);
        }

        optionCursers[pos].SetActive(true);

        optionLumSlide.value = gm.optionValues.lum;
        optionMainSound.value = gm.optionValues.mainSound;
        optionOtherSound.value = gm.optionValues.otherSound;
        optionMusicSound.value = gm.optionValues.musicSound;
        lightAdjustment.color = Color.Lerp(darkestLight, Color.clear, gm.optionValues.lum);
        
        //optionReso.text = gm.optionValues.resolutions[gm.optionValues.currentReso].width + " x " + gm.optionValues.resolutions[gm.optionValues.currentReso].height ;
    }

    void StartLevelSelect()
    {
        UpdateLevelSelect(0);
        for (int i = 0; i < levelRanks.Length; i++)
        {
            if (gm.ranks[i]<4)
            {
                levelRanks[i].sprite = rankSprites[gm.ranks[i]];
                levelRanks[i].color = Color.white;
            }
            else
            {
                levelRanks[i].color = Color.clear;
            }
        }
    }

    public void UpdateLevelSelect(int pos)
    {
        for (int i = 0; i < levelCursors.Length; i++)
        {
            levelCursors[i].SetActive(false);
        }
        levelCursors[pos].SetActive(true);
    }
    #endregion

    #region InGame

    public void SetLevel()
    {
        numberofMoves = 0;
        levelName.text = gm.levelData.levelNames[gm.currentLevel];
        UpdateMove(0);

    }

    public void SetupBlockVisuals(int[] baseValues)
    {
        for (int i = 0; i < baseValues.Length; i++)
        {
            blocksLeft[i].text = baseValues[i].ToString();
            if (baseValues[i] <= 0) blocksTypes[i].SetActive(false);
            else blocksTypes[i].SetActive(true);
        }
    }

    public void UpdateBlocksLeft(int[] values)
    {
        for (int i = 0; i < values.Length; i++)
        {
            blocksLeft[i].text = values[i].ToString();
        }
    }

    public void UpdateMove(int modif)
    {
        numberofMoves += modif;
        numberOfMovesText.text = numberofMoves.ToString();

        for (int i = 0; i < gm.levelData.rankTresholds[gm.currentLevel].thresholds.Length; i++)
        {
            if(numberofMoves < gm.levelData.rankTresholds[gm.currentLevel].thresholds[i])
            {
                currentRank.sprite = rankSprites[i];
                rank = i;
                break;
            }
        }

        if(numberofMoves> gm.levelData.rankTresholds[gm.currentLevel].thresholds[gm.levelData.rankTresholds[gm.currentLevel].thresholds.Length - 1])
        {
            currentRank.sprite = rankSprites[rankSprites.Length - 1];
        }
    }

    public void UpdateInGameMenu(int pos)
    {
        for (int i = 0; i < menuIGCursors.Length; i++)
        {
            menuIGCursors[i].SetActive(false);
        }
        menuIGCursors[pos].SetActive(true);
    }

    public void UpdateInGameOptions(int pos)
    {
        
        foreach (GameObject curse in inGameOptionCursers)
        {
            curse.SetActive(false);
        }

        inGameOptionCursers[pos].SetActive(true);

        optionIGLumSlide.value = gm.optionValues.lum;
        optionIGMainSound.value = gm.optionValues.mainSound;
        optionIGOtherSound.value = gm.optionValues.otherSound;
        optionIGMusicSound.value = gm.optionValues.musicSound;
        lightAdjustment.color = Color.Lerp(darkestLight, Color.clear, gm.optionValues.lum);
        //optionIGReso.text = gm.optionValues.resolutions[gm.optionValues.currentReso].width + " x " + gm.optionValues.resolutions[gm.optionValues.currentReso].height;
    }

    #endregion


    #region Dialogue

    public void StartDialogue(int gmNumber)
    {
        currentDialogue.text = "";
        dialogueUI.SetActive(true);
        gm.dm.StartDialogue(gmNumber);
    }
    public void ShowNextButton()
    {
        nextButton.SetActive(true);
    }

    public void HideNextButton()
    {
        nextButton.SetActive(false);
    }

    public void SetDialogue(Sprite spritePortrait)
    {
        currentDialogue.text = "";
        portrait.sprite = spritePortrait;
        for (int i = 0; i < portraitnames.pairings.Count; i++)
        {
            if (portraitnames.pairings[i].sprite == spritePortrait)
            {
                characterName.text = portraitnames.pairings[i].name;
            }
        }
    }


    public void HideChoice()
    {
        choice1text.text = "";
        choice2text.text = "";
        choiceUI.SetActive(false);
    }

    public void SetChoice(string text1, string text2)
    {
        choice1text.text = text1;
        choice2text.text = text2;
        choiceUI.SetActive(true);
    }

    public void UpdateCursorChoice(int pos)
    {
        if (pos == 1)
        {
            choiceCursor1.SetActive(false);
            choiceCursor2.SetActive(true);
        }
        else
        {
            choiceCursor1.SetActive(true);
            choiceCursor2.SetActive(false);
        }
    }

    public void EndDialogue()
    {
        currentDialogue.text = "";
        dialogueUI.SetActive(false);
    }
    #endregion

    #region EndLevel

    public void UpdateEndPos(int newPos)
    {
        foreach(GameObject curs in endLevelCursers)
        {
            curs.SetActive(false);
        }

        endLevelCursers[newPos].SetActive(true);
    }
    #endregion

    void ResetMenuPos()
    {
        UpdateInGameMenu(0);
        UpdateCursorChoice(0);
        UpdateEndPos(0);
        UpdateInGameOptions(0);
        UpdateLevelSelect(0);
        UpdateMainMenu(0);
        UpdateMainMenuOptions(0);
        optionManager.endLevelValue = 0;
        optionManager.inGameMenuValue = 0;
        optionManager.inGameValue = 0;
        optionManager.levelSelectValue = 0;
        optionManager.mainMenuOptionValue = 0;
        optionManager.mainMenuValue = 0;
    }
}

[System.Serializable]
public class LeanProfiles
{
    public string profileName;
    public LeanTweenType type;
    public float speed;
    public GameObject[] targets;
    //use X if float movement
    public Vector3 movement;
}

[System.Serializable]
public class groupImages
{
    public Image[] images;
}
