using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Names
{
    //Blocks
    public const string normalBlock = "normal,Block";
    public const string joyBlock = "joy,Block";
    public const string sadBlock = "sad,Block";
    public const string angryBlock = "angry,Block";
    public const string disgustBlock = "disgust,Block";
    public const string fearBlock = "fear,Block";

    //Ends
    public const string normalEnd = "normal,End";
    public const string joyEnd = "joy,End";
    public const string sadEnd = "sad,End";
    public const string angryEnd = "angry,End";
    public const string disgustEnd = "disgust,End";
    public const string fearEnd = "fear,End";

    //Gameplay
    public const string hole = "hole";

   //Walls
    public const string exteriorWall = "exterior";
    public const string interiorWall = "interior";
    public const string floor = "floor";

    //UI
    public const string inGameOptionsIN = "inGameOptionsIN";
    public const string inGameOptionsOUT = "inGameOptionsOUT";
    public const string levelSelectIN = "levelSelectIN";
    public const string levelSelectOUT = "levelSelectOUT";
    public const string mainMenuOptionsIN = "mainMenuOptionsIN";
    public const string mainMenuOptionsOUT = "mainMenuOptionsOUT";
    public const string mainMenuIN = "mainMenuIN";
    public const string mainMenuOUT = "mainMenuOUT";
} 
