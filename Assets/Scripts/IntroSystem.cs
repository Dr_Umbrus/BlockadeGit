using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroSystem : MonoBehaviour
{
    GameManager gm;
    public float timeToSkip;
    public SceneType next;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        timeToSkip -= Time.deltaTime;

        if(timeToSkip<=0 || Input.anyKeyDown)
        {
            gm.RequestLoad(next);
        }
    }
}
