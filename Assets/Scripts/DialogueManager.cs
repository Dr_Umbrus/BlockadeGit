using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public enum DialogueStates { Null, Dialogue, Choice };

    public DialogueStates state = DialogueStates.Null;
    public float timeBetweenLetters = .25f;
    public string currentMessage;
    public GameManager gm;
    UIManager uim;
    float waitingTime = 0;
    int currentCharacter = -1;
    public int currentMessageID = 0;

    public int currentGM = 0;

    int currentChoice = 0;


    // Start is called before the first frame update
    void Awake()
    {
        uim = GetComponent<UIManager>();
        gm = FindObjectOfType<GameManager>();
        waitingTime = timeBetweenLetters;
    }

    private void Update()
    {
        switch (state)
        {
            case DialogueStates.Null:
                break;
            case DialogueStates.Dialogue:
                CheckDialogue();
                break;
            case DialogueStates.Choice:
                CheckChoice();
                break;
        }


    }

    public void StartDialogue(int gmNumber)
    {
        currentGM = gmNumber;
        currentMessageID = 0;
        currentMessage = gm.scriptableText.dialogues[currentGM].text[currentMessageID];
        state = DialogueStates.Dialogue;
        SetUIDialogue();

        if (uim.state != UIManager.UIStates.Dialogue)
        {
            uim.state = UIManager.UIStates.Dialogue;
        }
    }

    public void EndDialogue()
    {
        currentMessageID = 0;
        state = DialogueStates.Null;
        if (uim.startingLevel)
        {
            uim.startingLevel = false;
            uim.ChangeState(UIManager.UIStates.InGame);
        }
        else
        {
            uim.ChangeState(UIManager.UIStates.EndLevel);
        }

    }

    void CheckDialogue()
    {
        if (uim.currentDialogue.text.Length != currentMessage.Length)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                waitingTime = 0;
                currentCharacter = currentMessage.Length - 1;
                uim.currentDialogue.text = currentMessage;
            }
            UpdateDialogue();
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (gm.scriptableText.dialogues[currentGM].nextDial[currentMessageID] != 0)
                {
                    currentMessageID = gm.scriptableText.dialogues[currentGM].nextDial[currentMessageID];
                    if (gm.scriptableText.dialogues[currentGM].isChoice[currentMessageID])
                    {
                        SetUIChoice();
                    }
                    else
                    {
                        currentMessage = gm.scriptableText.dialogues[currentGM].text[currentMessageID];
                        SetUIDialogue();
                    }
                }
                else
                {
                    EndDialogue();
                }
            }
        }
    }

    void SetUIDialogue()
    {
        currentCharacter = -1;
        state = DialogueStates.Dialogue;
        uim.SetDialogue(gm.scriptableText.dialogues[currentGM].portrait[currentMessageID]);

    }

    void SetUIChoice()
    {
        uim.currentDialogue.text = "";
        state = DialogueStates.Choice;
        uim.SetChoice(gm.scriptableText.dialogues[currentGM].text[currentMessageID], gm.scriptableText.dialogues[currentGM].text[currentMessageID + 1]);
        uim.UpdateCursorChoice(0);
        currentChoice = 0;
    }

    void CheckChoice()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (currentChoice == 0)
            {
                currentChoice = 1;
            }
            else
            {
                currentChoice--;
            }
            //uim.UpdateCursorChoice();
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (currentChoice == 1)
            {
                currentChoice = 0;
            }
            else
            {
                currentChoice++;
            }
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
           
            uim.HideChoice();
            if (gm.scriptableText.dialogues[currentGM].nextDial[currentMessageID + currentChoice] != 0)
            {
                currentMessageID = gm.scriptableText.dialogues[currentGM].nextDial[currentMessageID+currentChoice];
                if (gm.scriptableText.dialogues[currentGM].isChoice[currentMessageID])
                {
                    state = DialogueStates.Choice;
                    SetUIChoice();
                }
                else
                {
                    currentMessage = gm.scriptableText.dialogues[currentGM].text[currentMessageID];
                    uim.HideChoice();
                    SetUIDialogue();
                }
            }
            else
            {
                EndDialogue();
            }
        }

        if (Input.anyKeyDown)
        {
            uim.UpdateCursorChoice(currentChoice);
        }
    }

    void UpdateDialogue()
    {
        waitingTime -= Time.deltaTime;
        if (waitingTime <= 0)
        {
            if (currentCharacter < currentMessage.Length - 1)
            {
                currentCharacter++;
                uim.currentDialogue.text += currentMessage[currentCharacter];
            }
            waitingTime = timeBetweenLetters;
            if (currentCharacter == currentMessage.Length - 1)
            {
                uim.ShowNextButton();
            }
        }
    }
}
