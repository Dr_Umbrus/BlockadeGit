using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    SceneType currentScene=null;
    public int currentLevel;
    public int maxUnlockedLevel;
    public int[] ranks;
    public int lastLevel;

    public float blockSpeed;

    public UIManager uim;
    public Options oM;
    public DialogueManager dm;
    public GameData levelData;
    public ScriptableBlockData data;

    public TextAsset csv;
    public ScriptableTextData scriptableText;

    public ScriptableOptionValues optionValues;

    private void Awake()
    {
        optionValues.CheckRes();

        SceneManager.LoadSceneAsync(levelData.uIScene, LoadSceneMode.Additive);
        StartCoroutine(GrabUI());
        string[][] text = CsvParser2.Parse(csv.text);
        scriptableText.SetupData(text);
        if (ranks.Length < lastLevel+1)
        {
            ranks = new int[lastLevel+1];

            for (int i = 0; i < ranks.Length; i++)
            {
                ranks[i] = 5;
            }
        }
    }


    public void LoadScene(SceneType nextScene)
    {
        uim.sr = null;
        if (currentScene != null)
        {
            SceneManager.UnloadSceneAsync(currentScene.name);
        }
        SceneManager.LoadSceneAsync(nextScene.name, LoadSceneMode.Additive);
        currentScene = nextScene;
        switch (currentScene.type)
        {
            case SceneType.SceneTypes.MainMenu:
                uim.ChangeState(UIManager.UIStates.MainMenu);
                oM.mainMenuValue = 0;
                break;
            case SceneType.SceneTypes.Level:
                oM.levelSelectValue = 0;
                uim.startingLevel = true;
                uim.ChangeState(UIManager.UIStates.InGame);
                break;
            case SceneType.SceneTypes.Null:
                uim.ChangeState(UIManager.UIStates.Null);
                break;
            case SceneType.SceneTypes.Ending:
                uim.ChangeState(UIManager.UIStates.Null);
                break;
            case SceneType.SceneTypes.Start:
                uim.ChangeState(UIManager.UIStates.PressStart);
                break;
        }
    }

    public void ChangeResolution()
    {

    }
    

    IEnumerator GrabUI()
    {
        while (uim == null)
        {
            yield return 0;
            uim = FindObjectOfType<UIManager>();
        }

        oM = uim.GetComponent<Options>();
        dm = uim.GetComponent<DialogueManager>();
        RequestLoad(new SceneType(levelData.mainMenuScene, SceneType.SceneTypes.Start));
    }

    public void RequestLoad(SceneType nextScene)
    {
        StartCoroutine(WaitForLoad(nextScene));
    }

    IEnumerator WaitForLoad(SceneType nextScene)
    {
        uim.ChangeBlackening();
        yield return new WaitForSeconds(1 / uim.blackLerpSpeed);
        
        LoadScene(nextScene);
        yield return 0;
        while (SceneManager.sceneCount != 3)
        {
            yield return 0;
        }
        uim.ChangeBlackening();
    }
}

[System.Serializable]
public class SceneType
{
    public enum SceneTypes { Null, MainMenu, Level, Ending, Start}

    public string name;
    public SceneTypes type= SceneTypes.Null;

    public SceneType(string n, SceneTypes t)
    {
        name = n;
        type = t;
    }
}
