using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneReader : MonoBehaviour
{
    public GameObject[,] grid = new GameObject[0, 0];
    public List<BlockInfos> movingBlocks= new List<BlockInfos>();
    public List<EndInfos> endings = new List<EndInfos>();

    public EndInfosUI endingsForUI;

    public GameManager gm;

    public ScriptableBlockData data;

    public int x = 0;
    public int y = 0;

    public float movementLerp=1;
    public float speed = 1;

    UIManager uim;

    bool levelFinished = false;
    bool movingForward = true;

    private void Awake()
    {
        gm = FindObjectOfType<GameManager>();
        uim = FindObjectOfType<UIManager>();

        speed = gm.blockSpeed;

        uim.sr = this;
        data = gm.data;
              
        grid = new GameObject[x, y];

        for (int i = 0; i < transform.childCount; i++)
        {
            int u = (int)transform.GetChild(i).position.x;
            int v = (int)transform.GetChild(i).position.y;

            

            if (transform.GetChild(i).name.Contains("Block"))
            {
                movingBlocks.Add(new BlockInfos(new Vector2(u,v), transform.GetChild(i)));
                transform.GetChild(i).Translate(new Vector3(0, 0, -1));
                grid[u, v] = null;
            }

            else if (transform.GetChild(i).name.Contains("End"))
            {
                string[] partName = transform.GetChild(i).name.Split(',');

                string truename = partName[0] + ",Block";

                int pair = 0;
                if (data.pairings.Length < 1)
                {
                    data.WipeData();
                }
                for (int j = 0; j < data.pairings.Length; j++)
                {
                    if(data.pairings[j].name == transform.GetChild(i).name)
                    {
                        pair = j;
                    }
                }

                endings.Add(new EndInfos(new Vector2(u, v), transform.GetChild(i).gameObject, truename, data.pairings[pair].noEnd, data.pairings[pair].end));

               
                grid[u, v] = null;
            }

            else
            {
                grid[u, v] = transform.GetChild(i).gameObject;
            }
        }
    }

    
    private void Update()
    {
        if (uim.state==UIManager.UIStates.InGame)
        {
            if (movementLerp < 1)
            {
                movementLerp += Time.deltaTime * speed;

                for (int i = 0; i < movingBlocks.Count; i++)
                {
                    Vector2 pos = movingBlocks[i].currentPosition;
                    //pos = Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, movementLerp);
                    pos = nextPos(i);

                    movingBlocks[i].bloc.position = new Vector3(pos.x, pos.y, -1);
                }

                if (movementLerp > 1)
                {
                    movementLerp = 1;

                    for (int i = 0; i < movingBlocks.Count; i++)
                    {
                        if (movingForward)
                        {
                            movingBlocks[i].pastPositions.Add(movingBlocks[i].currentPosition);
                        }
                        else
                        {
                            movingBlocks[i].pastPositions.RemoveAt(movingBlocks[i].pastPositions.Count - 1);
                        }

                        movingBlocks[i].currentPosition = movingBlocks[i].futurePosition;
                        movingBlocks[i].check = false;
                        movingBlocks[i].loading = false;
                        movingBlocks[i].intermediaryPosition = movingBlocks[i].currentPosition;

                    }

                    movingForward = true;
                    bool levelComplete = true;

                    for (int i = 0; i < endings.Count; i++)
                    {

                        bool validation = false;

                        for (int j = 0; j < movingBlocks.Count; j++)
                        {

                            if (movingBlocks[j].currentPosition == endings[i].position && movingBlocks[j].bloc.name == endings[i].targetName)
                            {

                                validation = true;
                                if (!endings[i].validated) ValidateEnd(i);
                            }
                        }

                        if (!validation)
                        {
                            if (endings[i].validated) NoValidateEnd(i);
                            levelComplete = false;
                        }

                    }

                    if (levelComplete)
                    {
                        EndLevel();
                    }
                }
            }

            else if (!levelFinished)
            {
                if (Input.GetKeyDown(KeyCode.Z))
                {
                    GoBack();
                }
                else if (Mathf.RoundToInt(Input.GetAxis("Horizontal")) != 0)
                {
                    Movement(new Vector2(Mathf.RoundToInt(Input.GetAxis("Horizontal")), 0));
                }
                else if (Mathf.RoundToInt(Input.GetAxis("Vertical")) != 0)
                {
                    Movement(new Vector2(0, Mathf.RoundToInt(Input.GetAxis("Vertical"))));
                }


            }
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            EndLevel();
        }
    }

    public void EndLevel()
    {
        levelFinished = true;
        if (gm.currentLevel == gm.maxUnlockedLevel && (gm.currentLevel == gm.maxUnlockedLevel && gm.scriptableText.dialogues[gm.currentLevel * 2 + 1].text.Count > 0))
        {
            uim.ChangeState(UIManager.UIStates.Dialogue);
        }
        else
        {
            uim.ChangeState(UIManager.UIStates.EndLevel);
        }
    }

    #region mouvements

    public void GoBack()
    {
        if (movingBlocks[0].pastPositions.Count > 0)
        {
            uim.UpdateMove(-1);
            movementLerp = 0;
            for (int i = 0; i < movingBlocks.Count; i++)
            {
                movingBlocks[i].futurePosition = movingBlocks[i].pastPositions[movingBlocks[i].pastPositions.Count - 1];
            }
        }
        
    }

    public void Movement(Vector2 movement)
    {
        uim.UpdateMove(1);
        movementLerp = 0;
        for (int i = 0; i < movingBlocks.Count; i++)
        {
            movingBlocks[i].intermediaryPosition = CheckIntermediary(i, movement);
        }

        for (int i = 0; i < movingBlocks.Count; i++)
        {
            movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
        }
    }

    public Vector2 CheckIntermediary(int id, Vector2 movement)
    {
        switch (movingBlocks[id].bloc.name)
        {
            case Names.angryBlock:
                movement *= 2;
                break;
            case Names.disgustBlock:
                movement *= -1;
                break;
            case Names.joyBlock:
                movement *= Mathf.Max(x, y);
                break;
        }

        
        Vector2 theory = movingBlocks[id].currentPosition + movement;

        bool test = true;

        if((theory.x < 0 || theory.x >= x || theory.y < 0 || theory.y >= y) && movingBlocks[id].bloc.name != Names.joyBlock)
        {
            test = false;

        }
        else if(movingBlocks[id].bloc.name == Names.joyBlock)
        {
            if (movement.x == 0)
            {
                if (movement.y > 0)
                {
                    for (int i = 1; i<= (int)movement.y; i++)
                    {
                        if (grid[(int)theory.x, (int)movingBlocks[id].currentPosition.y+i] != null)
                        {
                            test = false;
                            movement.y = i;
                        }
                    }
                }
                else
                {
                    for (int i = -1; i >= (int)movement.y; i--)
                    {
                        if (grid[(int)theory.x, (int)movingBlocks[id].currentPosition.y + i] != null)
                        {
                            test = false;
                            movement.y = i;
                        }
                    }
                }
            }

            else if (movement.y == 0)
            {
                if (movement.x > 0)
                {
                    for (int i = 1; i <= (int)movement.x; i++)
                    {
                        if (grid[(int)movingBlocks[id].currentPosition.x + i, (int)theory.y] != null)
                        {

                            test = false;
                            movement.x = i;

                        }
                    }
                }
                else
                {
                    for (int i = -1; i >= (int)movement.x; i--)
                    {
                        if (grid[(int)movingBlocks[id].currentPosition.x + i, (int)theory.y] != null)
                        {

                            test = false;
                            movement.x = i;
                        }
                    }
                }
            }
            
        }

        else
        {
            if (grid[(int)theory.x, (int)theory.y] != null)
            {
                GameObject obstacle = grid[(int)theory.x, (int)theory.y];

                if (movingBlocks[id].bloc.name != Names.sadBlock && obstacle.name == Names.hole)
                {

                    test = false;
                }
                else if (obstacle.name == Names.exteriorWall || obstacle.name == Names.interiorWall)
                {

                    test = false;
                }
            }
        }

        while(!test)
        {
            test = true;
            if (movement.x < 0)
            {
                movement.x++;
            }
            else if (movement.x > 0)
            {
                movement.x--;
            }
            else if (movement.y < 0)
            {
                movement.y++;
            }
            else if (movement.y > 0)
            {
                movement.y--;
            }

            if(movement.x== 0 && movement.y == 0)
            {
                return movingBlocks[id].currentPosition;
            }
            else
            {
                theory = movingBlocks[id].currentPosition + movement;
            }

            if (theory.x < 0 || theory.x >= x || theory.y < 0 || theory.y >= y)
            {
                test = false;
            }
            else if (grid[(int)theory.x, (int)theory.y] != null)
            {
                GameObject obstacle = grid[(int)theory.x, (int)theory.y];

                if (movingBlocks[id].bloc.name != Names.sadBlock && obstacle.name == Names.hole)
                {
                    test = false;
                }
                else if (obstacle.name == Names.exteriorWall || obstacle.name == Names.interiorWall)
                {
                    test = false;
                }
            }

        }
        return theory;
    }

    public Vector2 CheckMovingBlocks(int id, Vector2 movement)
    {
        if (movingBlocks[id].check)
        {

            return movingBlocks[id].futurePosition;
        }
        else
        {
            if (!movingBlocks[id].loading)
            {
                movingBlocks[id].loading = true;
            }
            else
            {
                return movingBlocks[id].currentPosition;
            }
        }

        if(movingBlocks[id].currentPosition == movingBlocks[id].intermediaryPosition)
        {
            return movingBlocks[id].currentPosition;
        }

        

        switch (movingBlocks[id].bloc.name)
        {
            #region normal
            case Names.normalBlock:
                for (int i = 0; i < movingBlocks.Count; i++)
                {
                    if (i != id)
                    {
                        if(movingBlocks[i].currentPosition== movingBlocks[id].intermediaryPosition)
                        {
                            if (!movingBlocks[i].check)
                            {
                                movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                            }
                            

                            if(movingBlocks[i].futurePosition == movingBlocks[i].currentPosition)
                            {
                                movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;

                            }
                            else
                            {

                                movingBlocks[id].intermediaryPosition = movingBlocks[id].intermediaryPosition;
                            }
                        }
                    }
                }

                break;
            #endregion
            #region joy
            case Names.joyBlock:
                bool joyT = false;
                List<int> denied = new List<int>();

                while (!joyT)
                {
                    joyT = true;
                    if (movement.x > 0)
                    {
                        for (int i = 0; i < movingBlocks.Count; i++)
                        {
                            if (i != id && !denied.Contains(i))
                            {
                                
                                if (movingBlocks[i].currentPosition.y == movingBlocks[id].currentPosition.y && movingBlocks[i].currentPosition.x > movingBlocks[id].currentPosition.x && movingBlocks[i].currentPosition.x < movingBlocks[id].intermediaryPosition.x + 1)
                                {
                                    if (!movingBlocks[i].check)
                                    {
                                        movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                                    }
                                    if (movingBlocks[i].futurePosition.x - 1 <= movingBlocks[id].intermediaryPosition.x) movingBlocks[id].intermediaryPosition.x = movingBlocks[i].futurePosition.x - 1;
                                    denied.Add(i);
                                    joyT = false;
                                }
                            }
                        }
                    }
                    else if (movement.x < 0)
                    {
                        for (int i = 0; i < movingBlocks.Count; i++)
                        {
                            if (i != id && !denied.Contains(i))
                            {

                                if (movingBlocks[i].currentPosition.y == movingBlocks[id].currentPosition.y && movingBlocks[i].currentPosition.x < movingBlocks[id].currentPosition.x && movingBlocks[i].currentPosition.x > movingBlocks[id].intermediaryPosition.x - 1)
                                {
                                    if (!movingBlocks[i].check)
                                    {
                                        movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                                    }
                                    if (movingBlocks[i].futurePosition.x + 1 >= movingBlocks[id].intermediaryPosition.x) movingBlocks[id].intermediaryPosition.x = movingBlocks[i].futurePosition.x + 1;
                                    denied.Add(i);
                                    joyT = false;
                                }
                            }
                        }
                    }
                    else if (movement.y > 0)
                    {
                        for (int i = 0; i < movingBlocks.Count; i++)
                        {
                            if (i != id && !denied.Contains(i))
                            {
                                

                                if (movingBlocks[i].currentPosition.x == movingBlocks[id].currentPosition.x && movingBlocks[i].currentPosition.y > movingBlocks[id].currentPosition.y && movingBlocks[i].currentPosition.y < movingBlocks[id].intermediaryPosition.y + 1)
                                {

                                    

                                    if (!movingBlocks[i].check)
                                    {
                                        movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                                    }
                                    if(movingBlocks[i].futurePosition.y - 1<= movingBlocks[id].intermediaryPosition.y)  movingBlocks[id].intermediaryPosition.y = movingBlocks[i].futurePosition.y - 1;

                                    

                                    
                                    denied.Add(i);
                                    joyT = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < movingBlocks.Count; i++)
                        {
                            if (i != id && !denied.Contains(i))
                            {
                                if (movingBlocks[i].currentPosition.x == movingBlocks[id].currentPosition.x && movingBlocks[i].currentPosition.y < movingBlocks[id].currentPosition.y && movingBlocks[i].currentPosition.y > movingBlocks[id].intermediaryPosition.y - 1)
                                {
                                    if (!movingBlocks[i].check)
                                    {
                                        movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                                    }

                                    if (movingBlocks[i].futurePosition.y + 1 >= movingBlocks[id].intermediaryPosition.y) movingBlocks[id].intermediaryPosition.y = movingBlocks[i].futurePosition.y + 1;
                                    denied.Add(i);
                                    joyT = false;
                                }
                            }
                        }
                    }

                    movingBlocks[id].futurePosition = movingBlocks[id].intermediaryPosition;
                    
                }

                

                break;

            #endregion

            #region angry
            case Names.angryBlock:
                bool test = false;
                while (!test)
                {
                    test = true;
                    for (int i = 0; i < movingBlocks.Count; i++)
                    {
                        if (i != id)
                        {
                            if (movingBlocks[i].currentPosition == movingBlocks[id].intermediaryPosition)
                            {
                                if (!movingBlocks[i].check)
                                {
                                    movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                                }


                                if (movingBlocks[i].futurePosition == movingBlocks[i].currentPosition)
                                {
                                    if(Vector2.Distance(movingBlocks[id].intermediaryPosition, movingBlocks[id].currentPosition) > 1)
                                    {
                                        test = false;
                                        movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition + movement;
                                    }
                                    else
                                    {
                                        movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                                    }
                                }
                            }
                            else
                            {
                                if ((movingBlocks[i].bloc.name == Names.normalBlock || movingBlocks[i].bloc.name == Names.sadBlock || movingBlocks[i].bloc.name == Names.disgustBlock) && movingBlocks[i].intermediaryPosition == movingBlocks[id].intermediaryPosition)
                                {
                                    if (Vector2.Distance(movingBlocks[id].intermediaryPosition, movingBlocks[id].currentPosition) > 1)
                                    {
                                        test = false;
                                        movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition + movement;
                                    }
                                    else
                                    {
                                        movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                                    }
                                }
                            }
                        }

                        if (!test)
                        {
                            movingBlocks[id].intermediaryPosition = CheckIntermediary(id, movement/2);
                        }
                    }
                }
                
                break;
            #endregion

            #region disgust
            case Names.disgustBlock:

                for (int i = 0; i < movingBlocks.Count; i++)
                {
                    if (i != id)
                    {
                        if (movingBlocks[i].currentPosition == movingBlocks[id].intermediaryPosition)
                        {
                            if (!movingBlocks[i].check)
                            {
                                movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                            }


                            if (movingBlocks[i].futurePosition == movingBlocks[i].currentPosition)
                            {
                                movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                            }
                        }
                        else
                        {
                            if ((movingBlocks[i].bloc.name == Names.normalBlock || movingBlocks[i].bloc.name == Names.sadBlock) && movingBlocks[i].intermediaryPosition == movingBlocks[id].currentPosition + movement)
                            {
                                movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                            }
                        }
                    }
                }
                break;
            #endregion

            #region sad
            case Names.sadBlock:
                for (int i = 0; i < movingBlocks.Count; i++)
                {
                    if (i != id)
                    {
                        if (movingBlocks[i].currentPosition == movingBlocks[id].intermediaryPosition)
                        {
                            if (!movingBlocks[i].check)
                            {
                                movingBlocks[i].futurePosition = CheckMovingBlocks(i, movement);
                            }


                            if (movingBlocks[i].futurePosition == movingBlocks[i].currentPosition)
                            {
                                movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                            }
                        }
                        else
                        {
                            if(movingBlocks[i].bloc.name == Names.normalBlock && movingBlocks[i].intermediaryPosition== movingBlocks[id].intermediaryPosition)
                            {
                                movingBlocks[id].intermediaryPosition = movingBlocks[id].currentPosition;
                            }
                        }
                    }
                }
                break;
                #endregion
        }

        

        movingBlocks[id].check = true;
        return movingBlocks[id].intermediaryPosition;
    }


    Vector2 nextPos(int i)
    {
        if (movingBlocks[i].futurePosition == movingBlocks[i].currentPosition)
        {
            return movingBlocks[i].currentPosition;
        }
        else
        {
            //Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, movementLerp);
            switch (movingBlocks[i].bloc.name)
            {
                case Names.normalBlock:
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.normalCurve.Evaluate(movementLerp));
                case Names.joyBlock:
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.joyCurve.Evaluate(movementLerp));
                case Names.angryBlock:
                    movingBlocks[i].bloc.localScale = Vector3.Lerp(Vector3.one, gm.data.angerMaxSize, gm.data.angerSizeCurve.Evaluate(movementLerp));
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.angerCurve.Evaluate(movementLerp));
                case Names.sadBlock:
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.sadCurve.Evaluate(movementLerp));
                case Names.disgustBlock:
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.disgustCurve.Evaluate(movementLerp));
                case Names.fearBlock:
                    return Vector2.Lerp(movingBlocks[i].currentPosition, movingBlocks[i].futurePosition, gm.data.fearCurve.Evaluate(movementLerp));

            }
        }

        return Vector2.zero;
    }
    #endregion

    public void ValidateEnd(int end)
    {
        endings[end].validated = true;
        endings[end].meshRen.material = data.endMats[endings[end].end];
    }

    public void NoValidateEnd(int end)
    {
        endings[end].validated = false;
        endings[end].meshRen.material = data.endMats[endings[end].noEnd];
    }
}

public class BlockInfos
{
    public List<Vector2> pastPositions= new List<Vector2>();
    public Vector2 currentPosition;
    public Vector2 intermediaryPosition;
    public Vector2 futurePosition;
    public Transform bloc;
    public bool check = false;
    public bool loading = false;

    public BlockInfos(Vector2 startingPosition, Transform block)
    {
        currentPosition = startingPosition;
        intermediaryPosition = Vector2.zero;
        futurePosition = Vector2.zero;
        bloc = block;
    }
}

public class EndInfos
{
    public bool validated = false;
    public Vector2 position;
    public GameObject me;
    public string targetName;
    public MeshRenderer meshRen;
    public int noEnd;
    public int end;

    public EndInfos(Vector2 pos, GameObject obj, string target, int no, int yes)
    {
        position = pos;
        me = obj;
        targetName = target;
        meshRen = me.GetComponent<MeshRenderer>();
        noEnd = no;
        end = yes;
    }
}

public class EndInfosUI
{
    public EndInfos[] blockTypes = new EndInfos[6];
}