using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Data", fileName = "MainData", order = 1)]
public class GameData : ScriptableObject
{
    public List<string> levelNames = new List<string>();
    public string uIScene;
    public string mainMenuScene;
    public string endScene;

    
    public List<Thresholds> rankTresholds;

    [System.Serializable]
    public class Thresholds
    {
        public int[] thresholds = new int[4];
    }
}
