using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/BlockData", fileName = "BlockData", order = 3)]
public class ScriptableBlockData : ScriptableObject
{
    public Material[] endMats;
    public MaterialPairing[] pairings= new MaterialPairing[0];
    public string[] pairingNames = new string[5] { Names.normalEnd, Names.joyEnd, Names.sadEnd, Names.angryEnd, Names.disgustEnd};
    public AnimationCurve normalCurve, angerCurve, sadCurve, joyCurve, disgustCurve, fearCurve, angerSizeCurve;
    public Vector3 angerMaxSize;

    public void CheckData()
    {
        if (endMats.Length > 0)
        {
            int half = endMats.Length / 2;
            if (pairings.Length < half)
            {
                pairings = new MaterialPairing[half];

                for (int i = 0; i < pairings.Length; i++)
                {
                    pairings[i] = new MaterialPairing(pairingNames[i], i+half, i);
                }
            }
        }
    }

    public void WipeData()
    {
        pairings = new MaterialPairing[0];
        CheckData();
    }
}

public class MaterialPairing
{
    public string name;
    public int noEnd;
    public int end;

    public MaterialPairing(string nam, int no, int yes)
    {
        name = nam;
        noEnd = no;
        end = yes;
    }
}
