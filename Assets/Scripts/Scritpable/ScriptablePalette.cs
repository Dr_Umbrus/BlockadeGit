using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Palette", fileName = "Colors", order = 0)]
public class ScriptablePalette : ScriptableObject
{
    public Color[] col;
}
