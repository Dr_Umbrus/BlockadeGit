using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/PortraitNames", fileName = "PortraitNames", order = 6)]
public class PortraitNames : ScriptableObject
{
    public List<Association> pairings;

    [System.Serializable]
    public class Association
    {
        public Sprite sprite;
        public string name;
    }
}
