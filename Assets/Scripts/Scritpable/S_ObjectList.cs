using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Scriptables/ObjectListJSON", fileName = "ObjectListJSON", order = 0)]
public class S_ObjectList : ScriptableObject
{
    public ObjectPairing[] game = new ObjectPairing[0];
    public ObjectPairing[] intWalls = new ObjectPairing[0];
    public ObjectPairing[] extWalls = new ObjectPairing[0];

    public void ResetNames()
    {
        ObjectPairing[] newBlocks = new ObjectPairing[16]
        {
        new ObjectPairing(Names.exteriorWall),
        new ObjectPairing(Names.interiorWall),
        new ObjectPairing(Names.normalBlock),
        new ObjectPairing(Names.sadBlock),
        new ObjectPairing(Names.angryBlock),
        new ObjectPairing(Names.joyBlock),
        new ObjectPairing(Names.disgustBlock),
        new ObjectPairing(Names.fearBlock),
        new ObjectPairing(Names.normalEnd),
        new ObjectPairing(Names.sadEnd),
        new ObjectPairing(Names.angryEnd),
        new ObjectPairing(Names.joyEnd),
        new ObjectPairing(Names.disgustEnd),
        new ObjectPairing(Names.fearEnd),
        new ObjectPairing(Names.hole),
        new ObjectPairing(Names.floor),
        };

        for (int i = 0; i < game.Length; i++)
        {
            if (game[i].objects.Length > 0)
            {
                newBlocks[i].objects = game[i].objects;
            }
            newBlocks[i].correspondingID = game[i].correspondingID;
        }

        game = newBlocks;



        ObjectPairing[] newIntWalls = new ObjectPairing[14];

        for (int i = 0; i < newIntWalls.Length; i++)
        {
            
            newIntWalls[i] = new ObjectPairing(Names.interiorWall);
        }

        for (int i = 0; i < intWalls.Length; i++)
        {
            if (intWalls[i].objects.Length > 0)
            {
                newIntWalls[i].objects = intWalls[i].objects;
            }
            newIntWalls[i].correspondingID = newIntWalls[i].correspondingID;
        }

        intWalls = newIntWalls;

        ObjectPairing[] newExtWalls = new ObjectPairing[14];
        for (int i = 0; i < newExtWalls.Length; i++)
        {
            newExtWalls[i]=new ObjectPairing(Names.exteriorWall);
        }

        for (int i = 0; i < extWalls.Length; i++)
        {
            if (extWalls[i].objects.Length > 0)
            {
                newExtWalls[i].objects = extWalls[i].objects;
            }
            newExtWalls[i].correspondingID = newExtWalls[i].correspondingID;
        }

        extWalls = newExtWalls;
    }
}

[Serializable]
public class ObjectPairing
{
    public GameObject[] objects;
    public string correspondingName;
    public int correspondingID;

    public ObjectPairing(string naming)
    {
        correspondingName = naming;
        objects = new GameObject[0];
        correspondingID = -1;
    }

    public ObjectPairing(GameObject[] data, string naming, int id)
    {
        correspondingName = naming;
        objects = data;
        correspondingID = id;
    }
}
