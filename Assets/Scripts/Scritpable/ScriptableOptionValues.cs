using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Scriptables/Options", fileName = "ScriptableOptions", order = 7)]
public class ScriptableOptionValues : ScriptableObject
{
    
    public float lum = 1;
    public float mainSound = 1f;
    public float musicSound = .3f;
    public float otherSound = .3f;
    public int currentReso = 0;
    
    public Resolution[] resolutions;

    public void CheckRes()
    {
        resolutions = Screen.resolutions.Where(resolution => resolution.refreshRate == 60).ToArray();
    }

    public void LoadMe()
    {

    }
}
