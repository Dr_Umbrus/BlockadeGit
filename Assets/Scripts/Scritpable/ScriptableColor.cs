using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/Color", fileName = "color", order = 0)]
public class ScriptableColor : ScriptableObject
{
    public Color col;
}
