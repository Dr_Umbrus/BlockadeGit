using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptables/List", fileName = "ObjectList", order = 2)]
public class ObjectList : ScriptableObject
{
    public GameObject[] blocs;
    public ScriptableColor[] colors;
    public string[] names =
        {
        Names.exteriorWall,
        Names.interiorWall,
        Names.hole,
        Names.normalBlock,
        Names.joyBlock,
        Names.sadBlock,
        Names.angryBlock,
        Names.disgustBlock,
        Names.normalEnd,
        Names.joyEnd,
        Names.sadEnd,
        Names.angryEnd,
        Names.disgustEnd
        };


    public void UpdateNames()
    {
        names = new string[]
        {
        Names.exteriorWall,
        Names.interiorWall,
        Names.hole,
        Names.normalBlock,
        Names.joyBlock,
        Names.sadBlock,
        Names.angryBlock,
        Names.disgustBlock,
        Names.normalEnd,
        Names.joyEnd,
        Names.sadEnd,
        Names.angryEnd,
        Names.disgustEnd
        };
    }
}
