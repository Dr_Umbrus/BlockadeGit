using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(menuName = "Scriptables/TextData", fileName = "allText", order = 5)]
public class ScriptableTextData : ScriptableObject
{
    public List<string> misc;
    public List<string> levelDisplayNames;
    public List<Dialogue> dialogues;
    public int length1 = 0, length2 = 0; 

    [System.Serializable]
    public class Dialogue
    {
        public List<Sprite> portrait= new List<Sprite>();
        public List<int> nextDial = new List<int>();
        public List<string> text= new List<string>();
        public List<bool> isChoice= new List<bool>();

       
    }

    public void SetupData(string[][] text)
    {
        misc.Clear();
        levelDisplayNames.Clear();
        dialogues.Clear();

        for (int i = 1; i < text.Length; i++)
        {
            if (text[i][0] != "")
            {
                misc.Add(text[i][0]);
            }
        }


        for (int i = 1; i < text.Length; i++)
        {
            if (text[i][1] != "")
            {
                levelDisplayNames.Add(text[i][1]);
            }
        }


        for (int i = 2; i < text[0].Length; i+=4)
        {
            Dialogue dial= new Dialogue();
            for (int j = 1; j < text.Length; j++)
            {
                if (text[j][i] != "")
                {
                    dial.text.Add(text[j][i]);
                    
                    //dial.portrait.Add(AssetDatabase.LoadAssetAtPath<Sprite>("Assets/Resources/SpritesDialogue/" + text[j][i+1]));
                    dial.portrait.Add(Resources.Load<Sprite>("Assets/Resources/SpritesDialogue/" + text[j][i + 1]));
                    if (text[j][i+2] == "Y")
                    {
                        dial.isChoice.Add(true);
                    }
                    else
                    {
                        dial.isChoice.Add(false);
                    }
                    dial.nextDial.Add(int.Parse(text[j][i+3]));
                }
            }

            dialogues.Add(dial);
        }

        length1 = text.Length;
        length2 = text[0].Length;
    }
}
