using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Options : MonoBehaviour
{
    public int mainMenuValue = 0;
    public int maxMainMenuValue = 4;

    public int mainMenuOptionValue=0;
    public int maxMainMenuOption = 4;
    

    public int inGameValue=0;
    public int inGameMenuValue = 0;

    public int levelSelectValue = 0;
    public int levelSelectCollumn = 5;
    public int currentPage = 0;
    public int maxPage = 0;

    public int extraValue = 0;

    public int endLevelValue=0;

    public int reso = 0;


    public UIManager uim;
    public GameManager gm;


    private void Awake()
    {
        uim = GetComponent<UIManager>();
        gm = FindObjectOfType<GameManager>();
    }

    private void Update()
    {
        switch (uim.state)
        {
            case (UIManager.UIStates.Options):
                MainOptions();
                break;
            case (UIManager.UIStates.LevelSelect):
                LevelSelect();
                break;
            case (UIManager.UIStates.InGameOptions):
                InGameOptions();
                break;
            case (UIManager.UIStates.InGameMenu):
                InGameMenu();
                break;
            case (UIManager.UIStates.MainMenu):
                MainMenu();
                break;
            case (UIManager.UIStates.EndLevel):
                EndLevelOptions();
                break;
            case (UIManager.UIStates.InGame):
                InGame();
                break;
            case (UIManager.UIStates.PressStart):
                PressStart();
                break;
            case (UIManager.UIStates.Credits):
                CheckCredits();
                break;
        }
    }

    void CheckCredits()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.MainMenu);
        }
    }
    void PressStart()
    {
            if (Input.anyKeyDown)
            {
                uim.ChangeState(UIManager.UIStates.MainMenu);
            }
    }
    public void MainOptions()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            switch (mainMenuOptionValue)
            {
                case 0:
                    if (gm.optionValues.mainSound < 1)
                    {
                        gm.optionValues.mainSound += .05f;
                    }
                    break;
                case 1:
                    if (gm.optionValues.otherSound < 1)
                    {
                        gm.optionValues.otherSound += .05f;
                    }
                    break;
                case 2:
                    if (gm.optionValues.musicSound < 1)
                    {
                        gm.optionValues.musicSound += .05f;
                    }
                    break;
                case 3:
                    if (gm.optionValues.lum < 1)
                    {
                        gm.optionValues.lum += .05f;
                    }
                    break;
                case 4:
                    if (gm.optionValues.currentReso < gm.optionValues.resolutions.Length-1)
                    {
                        gm.optionValues.currentReso++;
                    }
                    else
                    {
                        gm.optionValues.currentReso = 0;
                    }
                    break;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            switch (mainMenuOptionValue)
            {
                case 0:
                    if (gm.optionValues.mainSound > 0)
                    {
                        gm.optionValues.mainSound -= .05f;
                    }
                    break;
                case 1:
                    if (gm.optionValues.otherSound >0)
                    {
                        gm.optionValues.otherSound -= .05f;
                    }
                    break;
                case 2:
                    if (gm.optionValues.musicSound >0)
                    {
                        gm.optionValues.musicSound -= .05f;
                    }
                    break;
                case 3:
                    if (gm.optionValues.lum > 0)
                    {
                        gm.optionValues.lum -= .05f;
                    }
                    break;
                case 4:
                    if (gm.optionValues.currentReso > 0)
                    {
                        gm.optionValues.currentReso--;
                    }
                    else
                    {
                        gm.optionValues.currentReso = gm.optionValues.resolutions.Length - 1;
                    }
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            mainMenuOptionValue--;
            if (mainMenuOptionValue < 0)
            {
                mainMenuOptionValue = maxMainMenuOption;
            }

            if (reso != gm.optionValues.currentReso)
            {
                reso = gm.optionValues.currentReso;
                gm.ChangeResolution();
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            mainMenuOptionValue++;
            if (mainMenuOptionValue > maxMainMenuOption)
            {
                mainMenuOptionValue = 0;
            }

            if (reso != gm.optionValues.currentReso)
            {
                reso = gm.optionValues.currentReso;
                gm.ChangeResolution();
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            uim.UpdateMainMenuOptions(mainMenuOptionValue);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.MainMenu);
        }
    }

    public void MainMenu()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            mainMenuValue++;
            if (mainMenuValue > maxMainMenuValue) mainMenuValue = 0;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            mainMenuValue--;
            if (mainMenuValue < 0) mainMenuValue = maxMainMenuValue;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (mainMenuValue)
            {
                case 0:
                    gm.RequestLoad(new SceneType(gm.levelData.levelNames[0], SceneType.SceneTypes.Level));
                    break;
                case 1:
                    uim.ChangeState(UIManager.UIStates.LevelSelect);
                    levelSelectValue = 0;
                    break;
                case 2:
                    uim.OpenMainMenuOptions();
                    break;
                case 3:
                    uim.ChangeState(UIManager.UIStates.Extra);
                    break;
                case 4:
                    Application.Quit();
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            uim.UpdateMainMenu(mainMenuValue);
        }
    }

    public void InGame()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.InGameMenu);
        }
    }

    public void InGameMenu()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            inGameMenuValue++;
            if (inGameMenuValue > 4) inGameMenuValue = 0;
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            inGameMenuValue--;
            if (inGameMenuValue < 0) inGameMenuValue = 4;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (inGameMenuValue)
            {
                case 0:
                    uim.ChangeState(UIManager.UIStates.InGame);
                    break;
                case 1:
                    uim.ChangeState(UIManager.UIStates.InGameOptions);
                    break;
                case 2:
                    gm.RequestLoad(new SceneType(gm.levelData.levelNames[gm.currentLevel], SceneType.SceneTypes.Level));
                    levelSelectValue = 0;
                    break;
                case 3:
                    gm.RequestLoad(new SceneType(gm.levelData.mainMenuScene, SceneType.SceneTypes.MainMenu));
                    break;
                case 4:
                    Application.Quit();
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            uim.UpdateInGameMenu(inGameMenuValue);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.InGame);
        }
    }

    public void InGameOptions()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            switch (inGameValue)
            {
                case 0:
                    if (gm.optionValues.mainSound < 1)
                    {
                        gm.optionValues.mainSound += .05f;
                    }
                    break;
                case 1:
                    if (gm.optionValues.otherSound < 1)
                    {
                        gm.optionValues.otherSound += .05f;
                    }
                    break;
                case 2:
                    if (gm.optionValues.musicSound < 1)
                    {
                        gm.optionValues.musicSound += .05f;
                    }
                    break;
                case 3:
                    if (gm.optionValues.lum < 1)
                    {
                        gm.optionValues.lum += .05f;
                    }
                    break;
                case 4:
                    if (gm.optionValues.currentReso < gm.optionValues.resolutions.Length - 1)
                    {
                        gm.optionValues.currentReso++;
                    }
                    else
                    {
                        gm.optionValues.currentReso = 0;
                    }
                    break;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            switch (mainMenuOptionValue)
            {
                case 0:
                    if (gm.optionValues.mainSound > 0)
                    {
                        gm.optionValues.mainSound -= .05f;
                    }
                    break;
                case 1:
                    if (gm.optionValues.otherSound > 0)
                    {
                        gm.optionValues.otherSound -= .05f;
                    }
                    break;
                case 2:
                    if (gm.optionValues.musicSound > 0)
                    {
                        gm.optionValues.musicSound -= .05f;
                    }
                    break;
                case 3:
                    if (gm.optionValues.lum > 1)
                    {
                        gm.optionValues.lum -= .05f;
                    }
                    break;
                case 4:
                    if (gm.optionValues.currentReso > 0)
                    {
                        gm.optionValues.currentReso--;
                    }
                    else
                    {
                        gm.optionValues.currentReso = gm.optionValues.resolutions.Length - 1;
                    }
                    break;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            inGameValue--;
            if (inGameValue < 0)
            {
                inGameValue = maxMainMenuOption;
            }

            if (reso != gm.optionValues.currentReso)
            {
                reso = gm.optionValues.currentReso;
                gm.ChangeResolution();
            }
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            inGameValue++;
            if (inGameValue > maxMainMenuOption)
            {
                inGameValue = 0;
            }

            if (reso != gm.optionValues.currentReso)
            {
                reso = gm.optionValues.currentReso;
                gm.ChangeResolution();
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow))
        {
            uim.UpdateInGameOptions(inGameValue);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.InGame);
        }
    }

    public void LevelSelect()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (gm.maxUnlockedLevel > levelSelectCollumn)
            {
                levelSelectValue -= levelSelectCollumn;
                if (levelSelectValue < 0)
                {
                    int reste = gm.maxUnlockedLevel % levelSelectCollumn;
                    levelSelectValue = gm.maxUnlockedLevel + levelSelectValue + reste;
                }
            }
            else
            {
                levelSelectValue = 0;
            }
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (gm.maxUnlockedLevel > levelSelectCollumn)
            {
                levelSelectValue += levelSelectCollumn;
                if (levelSelectValue > gm.maxUnlockedLevel)
                {
                    levelSelectValue = levelSelectValue % levelSelectCollumn;
                }
            }
            else
            {
                levelSelectValue = gm.maxUnlockedLevel;
            }
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (levelSelectValue > 0)
            {
                levelSelectValue--;
            }
            else
            {
                levelSelectValue = gm.maxUnlockedLevel;
            }
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(levelSelectValue< gm.maxUnlockedLevel)
            {
                levelSelectValue++;
            }
            else
            {
                levelSelectValue = 0;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            gm.currentLevel = levelSelectValue;
            gm.RequestLoad(new SceneType(gm.levelData.levelNames[levelSelectValue], SceneType.SceneTypes.Level));
        }

        if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.RightArrow))
        {
            uim.UpdateLevelSelect(levelSelectValue);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            uim.ChangeState(UIManager.UIStates.MainMenu);
            
        }
    }

    public void EndLevelOptions()
    {
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            endLevelValue++;
            if (endLevelValue > 2)
            {
                endLevelValue = 0;
            }
            uim.UpdateEndPos(endLevelValue);
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            endLevelValue--;
            if (endLevelValue < 0)
            {
                endLevelValue = 2;
            }
            uim.UpdateEndPos(endLevelValue);
        }



        if (Input.GetKeyDown(KeyCode.Space))
        {
            switch (endLevelValue)
            {
                case 0:
                    gm.currentLevel++;


                    if (gm.currentLevel > gm.maxUnlockedLevel)
                    {
                        if (gm.maxUnlockedLevel >= gm.lastLevel)
                        {

                            break;
                        }
                        else
                        {
                            gm.maxUnlockedLevel++;
                        }
                    }

                    gm.RequestLoad(new SceneType(gm.levelData.levelNames[gm.currentLevel], SceneType.SceneTypes.Level));
                    break;
                case 1:

                    if (gm.currentLevel == gm.maxUnlockedLevel)
                    {
                        if (gm.maxUnlockedLevel >= gm.lastLevel)
                        {

                            break;
                        }
                        else
                        {
                            gm.maxUnlockedLevel++;
                        }
                    }

                    gm.RequestLoad(new SceneType(gm.levelData.levelNames[gm.currentLevel], SceneType.SceneTypes.Level));
                    break;
                case 2:

                    if (gm.currentLevel == gm.maxUnlockedLevel)
                    {
                        if (gm.maxUnlockedLevel >= gm.lastLevel)
                        {

                            break;
                        }
                        else
                        {
                            gm.maxUnlockedLevel++;
                        }
                    }
                    gm.RequestLoad(new SceneType(gm.levelData.mainMenuScene, SceneType.SceneTypes.MainMenu));
                    break;
            }
        }
    }
}
