using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class LevelLoaderJson : EditorWindow
{
    public GameData data;
    public S_ObjectList objects;
    public string sceneName;
    public TextAsset jsonFile;
    public int targetPalette = 0;

    
    public Vector4 divs;
    int floorID = -1;

    [MenuItem("Tools/LevelCreatorJSON")]
    static void Init()
    {
        LevelLoaderJson loader = (LevelLoaderJson)EditorWindow.GetWindow(typeof(LevelLoaderJson));
        loader.Show();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("ResetNames"))
        {
            objects.ResetNames();
        }

        data = (GameData)EditorGUILayout.ObjectField("Scene List", data, typeof(GameData), false);

        objects = (S_ObjectList)EditorGUILayout.ObjectField("Object List", objects, typeof(S_ObjectList), false);

        sceneName = EditorGUILayout.TextField("Level Name", sceneName);

        
        divs = EditorGUILayout.Vector4Field("Camera Values", divs);
        GUI.Label(new Rect(0, 40, 100, 40), GUI.tooltip);

        jsonFile = (TextAsset)EditorGUILayout.ObjectField("JSON", jsonFile, typeof(TextAsset), false);

        if (GUILayout.Button("Generate Level"))
        {
            JSONRead();
        }

    }

    public void JSONRead()
    {
        if (jsonFile != null)
        {
            List<int> data=new List<int>();
            int width=0;
            int height=0;

            string json = jsonFile.text;

            string[] results = json.Split('{');
            string results2="";
            for (int i = 0; i < results.Length; i++)
            {
                if (results[i].Contains("data"))
                {
                    results2 = results[i];
                }
            }

            data=GetData(results2);

            for (int i = 0; i < results.Length; i++)
            {
                if (results[i].Contains("height"))
                {
                    results = results[i].Split(',');
                    break;
                }
            }

            for (int i = 0; i < results.Length; i++)
            {
                if (results[i].Contains("height"))
                {
                    results2 = results[i];
                }
            }

            height = GetLength(results2);

            for (int i = 0; i < results.Length; i++)
            {
                if (results[i].Contains("width"))
                {
                    results2 = results[i];
                }
            }

            width = GetLength(results2);

            int[,] finalLayout = new int[width, height];

            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    finalLayout[j, i] = data[j + width * (height-1-i)];
                }
            }

            GenerateLevel(finalLayout, width, height);
        }
    }

    public List<int> GetData(string results2)
    {
        string[] results = results2.Split(':');

        string[] data1 = results[1].Split('[');


        data1 = data1[1].Split(']');
        data1 = data1[0].Split(',');
        List<int> levelLayout = new List<int>();
        for (int i = 0; i < data1.Length; i++)
        {
            levelLayout.Add(int.Parse(data1[i]));
        }

        return levelLayout;
    }

    public int GetLength(string results2)
    {
        string[] results = results2.Split(':');
        return int.Parse(results[1]);
    }



    public void GenerateLevel(int[,] layout, int width, int height)
    {
        if (floorID < 0)
        {
            for (int i = 0; i < objects.game.Length; i++)
            {
                if (objects.game[i].correspondingName == Names.floor)
                {
                    floorID = objects.game[i].correspondingID;
                    break;
                }
            }
        }

        var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
        newScene.name = sceneName;

        

        GameObject par = new GameObject("root");

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                if(layout[j, i] != 0)
                {
                    for (int k = 0; k < objects.game.Length; k++)
                    {
                        if (objects.game[k].correspondingID == layout[j, i])
                        {
                            //Syst�me pour murs adaptatifs.

                            /*if (layout[j, i] == 1)
                            {
                                int wall = GetWall(layout, width, height, j, i);

                                //Mur ext�rieur wall
                                GameObject temp = Instantiate(Instantiate(objects.extWalls[wall].objects[targetPalette], new Vector2(j, i), Quaternion.identity, par.transform));
                                temp.name = objects.extWalls[wall].correspondingName;

                            }
                            else if (layout[j, i] == 2)
                            {
                                int wall = GetWall(layout, width, height, j, i);

                                //Mur int�rieur wall
                                GameObject temp = Instantiate(Instantiate(objects.intWalls[wall].objects[targetPalette], new Vector2(j, i), Quaternion.identity, par.transform));
                                temp.name = objects.intWalls[wall].correspondingName;
                            }
                            else
                            {*/
                            Quaternion angle;
                            if (objects.game[k].correspondingID < 15) angle = Quaternion.Euler(new Vector3(-180, 0, 0));
                            else angle = Quaternion.identity;


                                GameObject temp = Instantiate(objects.game[k].objects[targetPalette], new Vector2(j, i), angle, par.transform);
                            
                                temp.name = objects.game[k].correspondingName;
                            //}

                            if (objects.game[k].correspondingID >=3 )
                            {
                                temp = Instantiate(objects.game[2].objects[targetPalette], new Vector3(j, i, 2), Quaternion.Euler(new Vector3(-180, 0, 0)), par.transform);

                                temp.name = Names.floor;
                            }

                            break;
                        }
                        
                    }                 
                }
            }
        }

        par.AddComponent<SceneReader>();
        par.GetComponent<SceneReader>().x = width;
        par.GetComponent<SceneReader>().y = height;

        GameObject cam = new GameObject("Main Camera");
        cam.AddComponent<Camera>();
        Camera editCam = cam.GetComponent<Camera>();
        editCam.orthographic = true;

        if (height > width)
        {
            editCam.orthographicSize = height / divs.w;
        }
        else
        {
            editCam.orthographicSize = width / divs.z;
        }
        cam.transform.position = new Vector3(width / divs.x, height / divs.y, -5);
        editCam.clearFlags = CameraClearFlags.SolidColor;
        //editCam.backgroundColor = backgroundCol;
        data.levelNames.Add(sceneName);
        EditorSceneManager.SaveScene(newScene, Application.dataPath + "/Scenes/Levels/" + sceneName + ".unity", false);
    }

    public int GetWall(int[,] layout, int width, int height, int x, int y)
    {
        bool left=false, up=false, right=false, down=false;

        if(x!=0 && (layout[x-1, y]==1 || layout[x-1, y] == 2))
        {
            left = true;
        }
        if (y != 0 && (layout[x, y-1] == 1 || layout[x, y-1] == 2))
        {
            up = true;
        }
        if (x != width-1 && (layout[x + 1, y] == 1 || layout[x + 1, y] == 2))
        {
            right = true;
        }
        if (y !=height-1 && (layout[x, y+1] == 1 || layout[x, y+1] == 2))
        {
            down = true;
        }

        switch (left)
        {
            case true:
                switch (up)
                {
                    case true:
                        switch (right)
                        {
                            case true:
                                switch (down)
                                {
                                    case true:
                                        return 15;
                                    case false:
                                        return 11;
                                }

                            case false:
                                switch (down)
                                {
                                    case true:
                                        return 14;
                                    case false:
                                        return 5;
                                }
                        }
                    case false:
                        switch (right)
                        {
                            case true:
                                switch (down)
                                {
                                    case true:
                                        return 13;
                                    case false:
                                        return 9;
                                }

                            case false:
                                switch (down)
                                {
                                    case true:
                                        return 8;
                                    case false:
                                        return 2;
                                }
                        }
                }
            case false:
                switch (up)
                {
                    case true:
                        switch (right)
                        {
                            case true:
                                switch (down)
                                {
                                    case true:
                                        return 12;
                                    case false:
                                        return 6;
                                }
                            case false:
                                switch (down)
                                {
                                    case true:
                                        return 10;
                                    case false:
                                        return 4;
                                }
                        }
                    case false:
                        switch (right)
                        {
                            case true:
                                switch (down)
                                {
                                    case true:
                                        return 7;
                                    case false:
                                        return 1;
                                }

                            case false:
                                switch (down)
                                {
                                    case true:
                                        return 3;
                                    case false:
                                        return 0;
                                }
                        }
                }
        }
    }
}
