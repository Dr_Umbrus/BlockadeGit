using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;


public class LevelLoader : EditorWindow
{
    public GameData data;
    public ObjectList ol;
    Texture2D level;
    public string sceneName;
    public Color backgroundCol;

    public TextAsset json;

    [MenuItem("Tools/LevelCreator")]
    static void Init()
    {
        LevelLoader loader = (LevelLoader)EditorWindow.GetWindow(typeof(LevelLoader));
        loader.Show();
    }

    

    


    private void OnGUI()
    {
        data = (GameData)EditorGUILayout.ObjectField("Scene List", data, typeof(GameData), false);

        ol = (ObjectList)EditorGUILayout.ObjectField("List", ol, typeof(ObjectList), false);

        sceneName = EditorGUILayout.TextField("Level Name", sceneName);

        backgroundCol= EditorGUILayout.ColorField("Background", backgroundCol);

        level = (Texture2D)EditorGUILayout.ObjectField("Level", level, typeof(Texture2D), false);

        json= (TextAsset)EditorGUILayout.ObjectField("JSON", json, typeof(TextAsset), false);

        if (GUILayout.Button("Load"))
        {
            GenerateLevel();
        }

        if (GUILayout.Button("Read"))
        {
            ReadJson();
        }
    }

    void ReadJson()
    {
        string potato = json.text;
        Debug.Log(potato);
    }

    void GenerateLevel()
    {
        
        if (level != null)
        {

            var newScene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Single);
            newScene.name = sceneName;

            data.levelNames.Add(sceneName);

            GameObject par = new GameObject("root");

            for (int i = -1; i <= level.width; i++)
            {
                for (int j = -1; j <= level.height; j++)
                {
                    if(i==-1 || j==-1 || i==level.width || j == level.height)
                    {
                        GameObject temp = Instantiate(ol.blocs[0], new Vector2(i+1, j+1), Quaternion.identity, par.transform);
                        temp.name = ol.names[0];
                    }
                    else
                    {
                        //Debug.Log(level.GetPixel(i, j)+ " & neutral = " + ol.colors[3].col);
                        for (int c = 0; c < ol.colors.Length; c++)
                        {
                            bool test = true;

                            if(Mathf.Abs(level.GetPixel(i, j).b-ol.colors[c].col.b)>.2f || Mathf.Abs(level.GetPixel(i, j).r - ol.colors[c].col.r) > .2f || Mathf.Abs(level.GetPixel(i, j).g - ol.colors[c].col.g) > .2f)
                            {
                                test = false;
                            }

                            
                            if (test)
                            {
                                if(ol.colors[c].col.a <.8f && ol.colors[c].col.a > 0.2f && level.GetPixel(i, j).a >0.2f && level.GetPixel(i, j).a < .8f )
                                {
                                    GameObject temp = Instantiate(ol.blocs[c], new Vector2(i+1, j+1), Quaternion.identity, par.transform);
                                    temp.name = ol.names[c];
                                }
                                else if(ol.colors[c].col.a>.8f && level.GetPixel(i, j).a>.8f)
                                {
                                    GameObject temp = Instantiate(ol.blocs[c], new Vector2(i+1, j+1), Quaternion.identity, par.transform);
                                    temp.name = ol.names[c];
                                }
                            }
                        }
                    }

                }
            }

            par.AddComponent<SceneReader>();
            par.GetComponent<SceneReader>().x = level.width+2;
            par.GetComponent<SceneReader>().y = level.height+2;

            GameObject cam = new GameObject("Main Camera");
            cam.AddComponent<Camera>();
            Camera editCam = cam.GetComponent<Camera>();
            editCam.orthographic = true;

            if (level.height > level.width)
            {
                editCam.orthographicSize = level.height / 1.8f;
            }
            else
            {
                editCam.orthographicSize = level.width / 1.8f;
            }
            cam.transform.position = new Vector3(level.width / 1.8f, level.height/2.2f, -5);
            editCam.clearFlags = CameraClearFlags.SolidColor;
            editCam.backgroundColor = backgroundCol;

            EditorSceneManager.SaveScene(newScene, Application.dataPath+"/Scenes/Levels/"+sceneName+".unity", false);
        }

    }
}
